import React from "react";

import {
  Grommet as GrommetIcon,
  FacebookOption,
  Instagram,
  Twitter,
  Cubes,
  Favorite,
  Reactjs,
} from "grommet-icons";

import { Anchor, Box, Footer, grommet, Grommet, Main, Text } from "grommet";

const Media = () => (
  <Box direction="row" gap="xxsmall" justify="center">
    <Anchor
      a11yTitle="Share feedback on Github"
      href="https://www.instagram.com/"
      icon={<Instagram color="brand" />}
    />

    <Anchor
      a11yTitle="Chat with us on Slack"
      href="https://www.facebook.com/"
      icon={<FacebookOption color="brand" />}
    />

    <Anchor
      a11yTitle="Follow us on Twitter"
      href="https://twitter.com/"
      icon={<Twitter color="brand" />}
    />
  </Box>
);

function SiteFooter() {
  return (
    <Footer background="light-4" pad="small">
      <Box align="center" direction="row" gap="xsmall">
        <Cubes color="brand" size="medium" />

        <Text alignSelf="center" color="brand" size="small">
          benwal.io
        </Text>
      </Box>

      <Box direction="column" gap="xxsmall" align="center">
        <Text textAlign="center" size="xsmall">
          made with <Favorite color="red" size="small" /> in philly using:
        </Text>
        <Box direction="row" gap="xxsmall" justify="center">
          <Anchor
            a11yTitle="Link to Grommet"
            href="https://v2.grommet.io/"
            icon={<GrommetIcon color="brand" />}
          />
          <Anchor
            a11yTitle="Link to ReactJS"
            href="https://reactjs.org/"
            icon={<Reactjs color="brand" />}
          />
        </Box>
      </Box>

      <Text textAlign="center" size="xsmall">
        ©Copyright 2021
      </Text>
    </Footer>
  );
}

export default SiteFooter;
