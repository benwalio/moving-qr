import React, { createRef } from "react";
import SiteHeader from "../layout/SiteHeader";
import SiteFooter from "../layout/SiteFooter";
import nprogress from "nprogress";
import Router from "next/router";

function Layout({ children, user }) {
  const contextRef = createRef();

  Router.onRouteChange = () => nprogress.start();
  Router.onRouteChangeComplete = () => nprogress.done();
  Router.onRouteChangeError = () => nprogress.done();
  return (
    <>
      {user ? (
        <>
          <SiteHeader />
          {user ? <h1>logged in</h1> : <h1>no maracas</h1>}
          {children}
          <SiteFooter />{" "}
        </>
      ) : (
        <>
          {children}
          <SiteFooter />{" "}
        </>
      )}
    </>
  );
}

export default Layout;
