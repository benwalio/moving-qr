import React from "react";
import { Grommet, Header, Anchor, Box, ResponsiveContext, Menu } from "grommet";
import { Grommet as GrommetIcon, Menu as MenuIcon, Cubes } from "grommet-icons";
import Link from 'next/link';

function SiteHeader() {
  return (
    <Header background="light-4" pad="medium" height="xsmall">
      <Anchor
        href="https://tools.grommet.io/"
        icon={<Cubes color="brand" />}
        label="benwal moving app"
      />

      <ResponsiveContext.Consumer>
        {(size) =>
          size === "small" ? (
            <Box justify="end">
              <Menu
                a11yTitle="Navigation Menu"
                dropProps={{ align: { top: "bottom", right: "right" } }}
                icon={<MenuIcon color="brand" />}
                items={[
                  {
                    label: <Box pad="small">Grommet.io</Box>,

                    href: "https://v2.grommet.io/",
                  },

                  {
                    label: <Box pad="small">Feedback</Box>,

                    href: "https://github.com/grommet/grommet/issues",
                  },
                ]}
              />
            </Box>
          ) : (
            <Box justify="end" direction="row" gap="medium">
              <Anchor href="https://v2.grommet.io/" label="Grommet.io" />

              <Link href="https://github.com/grommet/grommet/issues">
                  <Anchor
                    
                    label="Feedback"
                  />
              </Link>
            </Box>
          )
        }
      </ResponsiveContext.Consumer>
    </Header>
  );
}

export default SiteHeader;
