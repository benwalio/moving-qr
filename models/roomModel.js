const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const roomSchema = new Schema(
  {
    name: { type: String},
    floor: { type: Number, default: 1},
    code: { type: String, unique: true},
    picUrl: { type: String },
    boxes: [{ box: { type: Schema.Types.ObjectId, ref: "box"}}]
  },
  { timestamps: true }
);

module.exports = mongoose.model("room", roomSchema);
