const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    name: { type: String},
    email: { type: String, unique: true },
    password: { type: String, required: true, select: false },
    username: { type: String, required: true, unique: true, trim: true },
    profilePicUrl: { type: String },
    newMsgPopup: { type: Boolean, default: true },
    unreadMsg: { type: Boolean, default: false },
    unreadNotification: { type: Boolean, default: false },
    role: { type: String, default: "user", enum: ["user", "root"] },
    resetToken: { type: String },
    expireToken: { type: Date },
  },
  { timestamps: true }
);

module.exports = mongoose.model("user", userSchema);
