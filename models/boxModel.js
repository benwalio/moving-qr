const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const boxSchema = new Schema(
  {
    boxid: { type: String },
    boxNumber: {type: Number},
    status: { type: String },
    picUrl: { type: String },
    flags: { isImportant: {type: Boolean, default: false}, isFavorite: {type : Boolean, default: false}},
    qrCode: {type:String},
    room: { type: Schema.Types.ObjectId, ref: "room"},
    contents: { text: {type: String}, value: {type: Number}}
  },
  { timestamps: true }
);

module.exports = mongoose.model("box", boxSchema);
