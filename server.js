const express = require("express");
const app = express();
const server = require("http").Server(app);
const next = require("next");
const dev = process.env.NODE_ENV !== "production";
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();
require("dotenv").config({ path: "./config.env" });
const connectDB = require("./utilsServer/connectDb");
const PORT = process.env.PORT || 3000;
connectDB();
app.use(express.json());

nextApp.prepare().then(() => {
    app.use("/api/signup", require("./api/signup"));
    app.use("/api/auth", require("./api/auth"));
    app.use("/api/room", require("./api/room"));
    app.use("/api/box", require("./api/box"));
    // app.use("/api/like", require("./api/posts/like"));
    app.all("*", (req, res) => handle(req, res));
    server.listen(PORT, (err) => {
      if (err) throw err;
      console.log(`express server running on ${PORT}`);
    });
  });