import React, {useState, useEffect} from 'react'
import { getBoxArray } from '../utils/boxUtils';
import {
    Box,
    Paragraph,
  } from "grommet";

function BoxData() {
    const [boxes, setBoxes] = useState([])

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getBoxArray();
            //   console.log(response);
            response.errors ? setErrorRequest(true) : setBoxes(response.array);
            // setRoomObj(response.obj);
          }
      
          fetchInitialData();
    },[])
    return (
        <>
        <Box fill gap="small" margin="small" align="center" justify="center">
            <Box width="large">
                
                    {boxes.map(box => {
                        <Paragraph>{box._id}</Paragraph>
                    })}
                
            </Box>
        </Box>
        </>
    )
}

export default BoxData
