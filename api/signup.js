const express = require("express");
const router = express.Router();
const userModel = require("../models/UserModel");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

router.get("/:username", async (req, res) => {
  const { username } = req.params;

  try {
    if (username.length < 1) return res.status(401).send("invalid username");

    const user = await userModel.findOne({ username: username.toLowerCase() });

    if (user) return res.status(401).send("invalid username");
    return res.status(200).send("username available");
  } catch (error) {
    console.error(error);
    return res.status(500).send("server error");
  }
});

router.post("/", async (req, res) => {
  const { name, email, username, password } = req.body.user;

  if (password.length < 6) res.status(401).send("invalid password");

  try {
    let user = await userModel.findOne({ username: username.toLowerCase() });
    if (user) return res.status(401).send("user already exists");
    user = new userModel({
      name,
      email: email.toLowerCase(),
      username: username.toLowerCase(),
      password
    });

    user.password = await bcrypt.hash(password, 10);
    await user.save();

    const payload = {
      userId: user._id,
    };

    jwt.sign(
      payload,
      process.env.jwtSecret,
      { expiresIn: "2d" },
      (err, token) => {
        if (err) throw err;
        res.status(200).json(token);
      }
    );
    
  } catch (error) {
    console.error(error);
    return res.status(500).send("server error");
  }
});

module.exports = router;
