const express = require("express");
const router = express.Router();
const authMiddleware = require("../middlewares/authMiddleware");
const boxModel = require("../models/boxModel");
const roomModel = require("../models/roomModel");
// const generateQR = require('../utils/genQR');
// import generateQR from '../utils/genQR';


router.post("/", authMiddleware, async (req, res) => {
    const { roomCode, status, picUrl, contents, value, flags } = req.body;
    console.log(req.body);
    if (roomCode.length < 1) return res.status(401).send("missing room");
  
    try {
      const newBox = {
        status: status,
        contents: {}
      };
      const room = await roomModel.find({ code: roomCode })
    //   let code = boxCode(name, floor);
      // console.log(code);
      newBox.room = room[0]._id;
    //   console.log(room[0]._id);
      if (picUrl) newBox.picUrl = picUrl;
      if (contents) newBox.contents.text = contents;
      if (value) newBox.contents.value = value;
      if (flags) newBox.contents.flags = flags;
  
      const box = await new boxModel(newBox).save();

      await room[0].boxes.unshift({box: box._id})
      await room[0].save()
    //   const qr = await generateQR("test");
    //   console.log(qr);

      box.boxNumber = room[0].boxes.length;

      box.boxid = box.boxNumber.toString().padStart(3,'0') + '-' + roomCode;

      await box.save()
  
      return res.status(200).json(box);
    } catch (error) {
      console.error(error);
      return res.status(500).send("server error");
    }
  });
  
  router.get("/", authMiddleware, async (req, res) => {
    try {
      let results = await boxModel.find().populate('room')
      return res.status(200).json(results);
      
    } catch (error) {
      console.error(error);
      return res.status(500).send("server error");
    }
  
  })
  
  router.get("/:boxId", authMiddleware, async (req, res) => {
    try {
      const {boxId} = req.params;
      let results = await boxModel.findById(boxId).populate('room')
      return res.status(200).json(results);
      
    } catch (error) {
      console.error(error);
      return res.status(500).send("server error");
    }
  })
  
  router.put("/:boxId", authMiddleware, async (req, res) => {
  
  })
  
  router.delete("/:boxId", authMiddleware, async (req, res) => {
    try {
      const {boxId} = req.params;
      let results = await boxModel.findById(boxId).populate('boxes')
      await results.remove()
      return res.status(200).json('box deleted');
      
    } catch (error) {
      console.error(error);
      return res.status(500).send("server error");
    }
  })
  
  module.exports = router;