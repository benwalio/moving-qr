const express = require("express");
const router = express.Router();
const userModel = require("../models/UserModel");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const authMiddleware = require("../middlewares/authMiddleware");
// const corsMiddleware = require("../middlewares/corsMiddleware")

router.get("/", authMiddleware, async (req, res) => {
  const {userId} = req

  try {
    const user = await userModel.findById(userId)

    return res.status(200).json({ user })
  } catch (error) {
    console.error(error);
    return res.status(500).send('server error')
    
  }
})

router.post("/", async (req, res) => {
  const {
    username,
    password,
  } = req.body.user;

  if (password.length < 6) res.status(401).send("invalid password");

  try {
    let user = await userModel.findOne({ username: username.toLowerCase() }).select("+password");
    if (!user) return res.status(401).send("invalid credentials");

    const isPassword = await bcrypt.compare(password,user.password)
    if(!isPassword) return res.status(401).send("invalid credentials")

    const payload = {
      userId: user._id,
    };
    jwt.sign(
      payload,
      process.env.jwtSecret,
      { expiresIn: "4h" },
      (err, token) => {
        if (err) throw err;
        res.status(200).json(token);
      }
    );
  } catch (error) {
    console.error(error);
    return res.status(500).send("server error");
  }
});

module.exports = router;
