const express = require("express");
const router = express.Router();
const authMiddleware = require("../middlewares/authMiddleware");
const roomCode = require("../utils/roomCode");
const roomModel = require("../models/roomModel");

router.post("/", authMiddleware, async (req, res) => {
  const { name, floor, picUrl } = req.body;

  if (name.length < 1) return res.status(401).send("missing name");

  try {
    const newRoom = {
      name: name,
      floor: floor,
    };
    let code = roomCode(name, floor);
    // console.log(code);
    newRoom.code = code;
    if (picUrl) newRoom.picUrl = picUrl;

    const room = await new roomModel(newRoom).save();

    return res.status(200).json(room);
  } catch (error) {
    console.error(error);
    return res.status(500).send("server error");
  }
});

router.get("/", authMiddleware, async (req, res) => {
  try {
    let results = await roomModel.find().populate('boxes')
    return res.status(200).json(results);
    
  } catch (error) {
    console.error(error);
    return res.status(500).send("server error");
  }

})

router.get("/:roomId", authMiddleware, async (req, res) => {
  try {
    const {roomId} = req.params;
    let results = await roomModel.findById(roomId).populate('boxes')
    return res.status(200).json(results);
    
  } catch (error) {
    console.error(error);
    return res.status(500).send("server error");
  }
})

router.put("/:roomId", authMiddleware, async (req, res) => {

})

router.delete("/:roomId", authMiddleware, async (req, res) => {
  try {
    const {roomId} = req.params;
    let results = await roomModel.findById(roomId).populate('boxes')
    await results.remove()
    return res.status(200).json('room deleted');
    
  } catch (error) {
    console.error(error);
    return res.status(500).send("server error");
  }
})

module.exports = router;