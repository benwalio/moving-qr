import { render, screen } from '@testing-library/react';
import Grommet from './App';

test('renders learn react link', () => {
  render(<Grommet />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
