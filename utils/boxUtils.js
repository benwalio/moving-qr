import axios from 'axios'
import baseUrl from './baseUrl'
import cookie from 'js-cookie'

const url = axios.create({
    baseURL: baseUrl,
    headers: {"Authorization": cookie.get('token')}
});

export const calcBoxId = async (room) => {

}

export const createBox = async(box) => {
    console.log(box);
    let { roomCode, status } = box
    let contents = box.contents.text;
    let value = box.contents.value;
    let result = url
        .post('/api/box', {roomCode, status, contents, value })
        .then(res => {
            console.log(res);
        })
    return result
}

export const getRoomArray = async () => {
    let result = url
        .get('/api/room')
        .then(res => {
            let roomArray = []
            let roomObj = res.data
            res.data.map(room => {
                roomArray.unshift(room.name)
            })
            
            return {array: roomArray, obj: roomObj}
        }).catch((error) => {
            return error;
        });

    return result
}

export const getBoxArray = async() => {
    let result = url
        .get('/api/box')
        .then(res => {
            let boxArray = []
            res.data.map(box => {
                boxArray.unshift(box)
            })
            return {array: boxArray}
        }).catch((error) => {
            return error;
        });
    return result;
}

// export const getRoomCode= async (room) => {
//     let result = url
//         .get(/api/room)
// }