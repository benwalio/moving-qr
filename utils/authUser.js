import axios from "axios";
import baseUrl from "./baseUrl";
import catchErrors from "./catchErrors";
import Router from "next/router";
import cookie from "js-cookie";

// export const registerUser = async (
//   user,
//   profilePicUrl,
//   setError,
//   setLoading
// ) => {
//     // console.log(user + "!!hiya!!")
//     setLoading(true)
//   try {
//     const res = await axios.post(`${baseUrl}/api/signup`, {
//       user,
//       profilePicUrl,
//     });
//     setToken(res.data);
//   } catch (error) {
//     const errorMsg = catchErrors(error);
//     setError(errorMsg);
//   }
//   setLoading(false)
// };

export const redirectUser = async (ctx, location) => {
  // if they're coming from server side, 302 redirect them
  if(ctx.req) {
    ctx.res.writeHead(302,{Location: location})
    ctx.res.end()
  } else { // else, frontend redirect them via the Router
    Router.push(location)
  }
}

export const loginUser = async (user, setError, setLoading) => {
  setLoading(true);
  try {
    const res = await axios.post(`${baseUrl}/api/auth`, { user, headers: {"Access-Control-Allow-Origin": "*" }});
    setToken(res.data);
  } catch (error) {
    const errorMsg = catchErrors(error);
    setError(errorMsg);
  }
  setLoading(false)
};

const setToken = (token) => {
  cookie.set("token", token);
  Router.push("/");
};

export const logoutUser = (email) => {
  cookie.set("userEmail", email)
  cookie.remove("token")
  Router.push("/login")
}
