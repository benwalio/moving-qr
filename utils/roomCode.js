function roomCode(name, floor) {
  let digit;

  // switch(name.includes()) {
  //     case "bed":
  //         digit = "BED"
  //         break
  //     case "bath":
  //         digit = "BAT"
  //         break
  //     case "kitc":
  //         digit = "KIT"
  //         break
  //     case "gar":
  //         digit = "GRG"
  //         break
  //     case "clo":
  //         digit = "CLO"
  //         break
  //     case "den":
  //         digit = "LVR"
  //         break
  //     case "livi":
  //         digit = "LVR"
  //         break
  //     case "dini":
  //         digit = "DNR"
  //         break
  //     case "base":
  //         digit = "BSM"
  //         break
  //     case "off":
  //         digit = "OFF"
  //         break
  //     case "ndry":
  //         digit = "LAU"
  //         break
  //     case "yar":
  //         digit = "YRD"
  //         break
  //     case "uest":
  //         digit = "GST"
  //         break
  //     default:
  //         digit = "MSC"

  if (name.includes("bath")) {
    digit = "BTH";
  } else if (name.includes("bed")) {
    digit = "BED";
  } else if (name.includes("kitc")) {
    digit = "KIT";
  } else if (name.includes("rag")) {
    digit = "GRG";
  } else if (name.includes("clo")) {
    digit = "CLO";
  } else if (name.includes("den") || name.includes("liv")) {
    digit = "LVR";
  } else if (name.includes("dini")) {
    digit = "DNR";
  } else if (name.includes("base")) {
    digit = "BMT";
  } else if (name.includes("off")) {
    digit = "OFF";
  } else if (name.includes("ndry")) {
    digit = "LAU";
  }else if (name.includes("yar")) {
    digit = "YAR";
  }else if (name.includes("ues")) {
    digit = "GBR";
  } else {
    digit = "MSC";
  }

  return floor + digit;
}

module.exports = roomCode;
