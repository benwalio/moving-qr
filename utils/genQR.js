import QRCode from 'qrcode'

// const generateQR = async text => {
//     try {
//         const qr = await QRCode.toDataURL(text, { errorCorrectionLevel: 'H' })
//       console.log(qr)
//       return qr
//     } catch (err) {
//       console.error(err)
//       return
//     }
//   }

// export default generateQR;

function generateQR (text) {
    try {
        const qr = await QRCode.toDataURL(text, { errorCorrectionLevel: 'H' })
      console.log(qr)
      return qr
    } catch (err) {
      console.error(err)
      return
    }
  }

export default generateQR;