import baseUrl from "../utils/baseUrl";
import axios from "axios";
import React, { useState, useEffect } from "react";
import { Form, FormField, TextInput, Button, Box } from "grommet";
import { Lock, Unlock, User } from "grommet-icons";
import {loginUser} from '../utils/authUser';

import cookie from "js-cookie";

function login() {
  const [user, setUser] = useState({
    username: "",
    password: "",
  });
  const [showPassword, setShowPassword] = useState(false);
  const [formLoading, setFormLoading] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");
  const [submitDisabled, setSubmitDisabled] = useState(true);

  const { username, password } = user;

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser((prev) => ({ ...prev, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault;
    await loginUser(user, setErrorMsg, setFormLoading);
  };

  return (
    <>
      <Form onSubmit={handleSubmit}>
        <Box align="center" pad="medium">
          <FormField>
            <Box direction="row" fill="horizontal" round="small" border>
              <TextInput
                plain
                name="username"
                value={username}
                onChange={handleChange}
                icon={<User />}
                reverse
                placeholder="username"
              />
            </Box>
          </FormField>
          <FormField>
            <Box direction="row" round="small" border>
              <TextInput
                plain
                name="password"
                value={password}
                onChange={handleChange}
                type={showPassword ? "text" : "password"}
                placeholder="password"
              />
              <Button
                icon={showPassword ? <Unlock /> : <Lock />}
                onClick={() => setShowPassword(!showPassword)}
              />
            </Box>
          </FormField>

          <Button primary label="login" type="submit" />
        </Box>
      </Form>
    </>
  );
}

export default login;
