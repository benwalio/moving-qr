import baseUrl from "../utils/baseUrl";
import axios from "axios";
import React, { useState, useEffect } from "react";
import {
  Form,
  FormField,
  Select,
  TextInput,
  Button,
  Box,
  TextArea,
  CheckBox,
  MaskedInput,
  Card,
  CardHeader,
  CardBody,
  Layer,
  Spinner,
  Text,
} from "grommet";
import { Lock, Unlock, User } from "grommet-icons";
import { getRoomArray, createBox } from "../utils/boxUtils";
import BoxData from '../components/BoxData';

export default function box() {
  const [loading, setLoading] = useState(false);
  const [showNewBox, setShowNewBox] = useState(true);
  const [isImportant, setIsImportant] = useState(false);
  const [isFavorite, setIsFavorite] = useState(false);
  const [boxStatus, setBoxStatus] = useState("not started");
  const [boxNo, setBoxNo] = useState(0);
  const [boxId, setBoxId] = useState("");
  const [roomObj, setRoomObj] = useState([]);
  const [roomCode, setroomCode] = useState("");
  const [rooms, setRooms] = useState([]);
  const [room, setRoom] = useState("");
  const [contentsText, setContentsText] = useState("");
  const [contentsValue, setContentsValue] = useState(0);
  const [errorRequest, setErrorRequest] = useState(false);
  const [box, setBox] = useState({
    status: boxStatus,
    contents: {
      text: contentsText,
      value: contentsValue,
    },
    flags: {
      isImportant: isImportant,
      isFavorite: isFavorite,
    },
    boxNumber: boxNo,
    boxid: boxId,
    roomCode: roomCode,
  });

  const statusObj = {
    "not started": {
      shortCode: "NS",
      sortId: 1,
    },
    "in progress": {
      shortCode: "IP",
      sortId: 2,
    },
    packed: {
      shortCode: "pk",
      sortId: 3,
    },
    staged: {
      shortCode: "SG",
      sortId: 4,
    },
    loaded: {
      shortCode: "LD",
      sortId: 5,
    },
    delivered: {
      shortCode: "DV",
      sortId: 6,
    },
    unpacked: {
      shortCode: "UP",
      sortId: 7,
    },
  };

  const handleChange = (e) => {
    // setLoading(true);
    const { name, value } = e.target;
    // setUser((prev) => ({ ...prev, [name]: value }));

    // setLoading(false);
  };

  useEffect(() => {
    async function fetchInitialData() {
      const response = await getRoomArray();
      //   console.log(response);
      response.errors ? setErrorRequest(true) : setRooms(response.array);
      setRoomObj(response.obj);
    }

    fetchInitialData();
  }, []);

  useEffect(() => {
    setBox({
      status: boxStatus,
      contents: {
        text: contentsText,
        value: contentsValue,
      },
      flags: {
        isImportant: isImportant,
        isFavorite: isFavorite,
      },
      boxNumber: boxNo,
      boxid: boxId,
      roomCode: roomCode,
    });
  }, [
    boxStatus,
    contentsText,
    contentsValue,
    isImportant,
    isFavorite,
    boxNo,
    boxId,
    roomCode,
  ]);

  useEffect(() => {
    console.log("ran");
    function getBoxNum() {
      roomObj.map((roomItm) => {
        //   console.log((roomItm.boxes.length + 1).toString().padStart(3, "0") + "-" + roomItm.code)
        roomItm.name === room && setBoxNo(roomItm.boxes.length + 1);
        roomItm.name === room && setroomCode(roomItm.code);
        roomItm.name === room &&
          setBoxId(
            (roomItm.boxes.length + 1).toString().padStart(3, "0") +
              "-" +
              roomItm.code
          );
        // roomItm.name === room && setBox({boxId: ((roomItm.boxes.length + 1).toString().padStart(3, "0") + "-" + roomItm.code), boxNo: (roomItm.boxes.length + 1), roomCode: roomItm.code, ...box })
      });
    }
    getBoxNum();
  }, [room]);

  //   useEffect(() => {
  //     async function getBoxId () {
  //     roomObj.map((roomItm) => {
  //       roomItm.name === room &&
  //         setBoxId(boxNo.toString().padStart(3, "0") + "-" + roomItm.code);
  //     });
  // }

  //     getBoxId ();
  //   }, [boxNo, room]);

  const handleSubmit = (e) => {
    setLoading(true);
    createBox(box);

    setTimeout(() => {
      setRoom("");
      setIsImportant(false);
      setIsFavorite(false);
      setBoxStatus("not started");
      setContentsText("");
      setContentsValue(0);
      setBoxId("");
      setLoading(false);
    }, 1000);
  };

  return (
    <>
      {" "}
      <Button
        primary
        label="create new box"
        onClick={() => setShowNewBox(!showNewBox)}
      />
      {showNewBox ? (
        <>
          <Box fill align="center" justify="center">
            <Box width="medium">
              <Card background="light-2">
                <CardHeader pad="medium" justify="center">
                  create a new box
                </CardHeader>
                <CardBody pad="small">
                  <Form
                    // onChange={(value) => console.log(value)}
                    onReset={() => {
                      setIsImportant(false);
                      setIsFavorite(false);
                      setBoxStatus("not started");
                      setContentsText("");
                      setContentsValue(0);
                      setBoxId("");
                    }}
                    onSubmit={(e) => handleSubmit(e)}
                  >
                    <Box direction="row-responsive" justify="evenly">
                      <FormField name="isImportant">
                        <CheckBox
                          name="isImportant"
                          label="important?"
                          checked={isImportant}
                          onChange={(event) =>
                            setIsImportant(event.target.checked)
                          }
                        />
                      </FormField>
                      <FormField name="isFavorite">
                        <CheckBox
                          name="isFavorite"
                          label="favorite?"
                          checked={isFavorite}
                          onChange={(event) =>
                            setIsFavorite(event.target.checked)
                          }
                        />
                      </FormField>
                    </Box>

                    <FormField label="boxStatus" name="boxStatus">
                      <Select
                        name="boxStatus"
                        options={Object.keys(statusObj)}
                        value={boxStatus}
                        onChange={(event) => setBoxStatus(event.option)}
                      />
                    </FormField>

                    <FormField label="room" name="room">
                      <Select
                        name="room"
                        options={rooms}
                        value={room}
                        onChange={(event) => setRoom(event.option)}
                      />
                    </FormField>

                    <FormField label="contents" name="contentsText">
                      <TextArea
                        name="contentsText"
                        value={contentsText}
                        onChange={(event) =>
                          setContentsText(event.target.value)
                        }
                      />
                    </FormField>

                    <Box
                      direction="row-responsive"
                      justify="around"
                      align="center"
                    >
                      <FormField label="contents value" name="contentsValue">
                        <MaskedInput
                          mask={[
                            {
                              regexp: /^[0-9]{1,10}$/,
                              placeholder: "150",
                            },
                          ]}
                          name="contentsValue"
                          value={contentsValue}
                          onChange={(event) =>
                            setContentsValue(event.target.value)
                          }
                        />
                      </FormField>
                      <FormField label="box ID">
                        <TextInput
                          name="boxid"
                          value={boxId || ""}
                          textAlign="center"
                          plain
                          disabled
                        />
                      </FormField>
                    </Box>

                    <Box
                      direction="row"
                      justify="between"
                      margin={{ top: "medium" }}
                    >
                      <Button label="Cancel" />

                      <Button type="reset" label="Reset" />

                      <Button type="submit" label="Submit" primary />
                    </Box>
                  </Form>
                </CardBody>
              </Card>
            </Box>
          </Box>
          {loading && (
            <Layer>
              <Box
                align="center"
                justify="center"
                gap="small"
                direction="row"
                alignSelf="center"
                pad="large"
              >
                <Spinner />

                <Text>creating new box...</Text>
              </Box>
            </Layer>
            
          )}
          <>
            <BoxData />
            </>
        </>
      ) : (
        <>
            <BoxData />
        </>
      )}
    </>
  );
}
