import App from "next/app";
import {Grommet, Box} from 'grommet'
// import "semantic-ui-css/semantic.min.css";
import axios from 'axios'
import { parseCookies, destroyCookie } from 'nookies';
import baseUrl from '../utils/baseUrl';
import { redirectUser } from '../utils/authUser'
import Layout from '../layout/Layout';

const theme = {
    global: {
      font: {
        family: 'Roboto',
        size: '18px',
        height: '20px',
      },
    },
    formField : {
      border: false
    }
  };

class MyApp extends App {
//   static async getInitialProps(appContext) {
    
//     const { Component, ctx } = appContext
//     let pageProps = {}

//     // look for jwt token in the cookie
//     const { token } = parseCookies(ctx)
//     const protectedRoutes = ctx.pathname === "/"

//       // if no token (not logged in) redirect them to login
//     if (!token) {
//       protectedRoutes && redirectUser(ctx, "/login")
//     } else { // else - allow them to get props
//     if(Component.getInitialProps) {
//       pageProps = await Component.getInitialProps(ctx)
//     }

//     try {
//       const res = await axios.get(`${baseUrl}/api/auth`,{headers:{Authorization: token}})
//       const { user, userFollowerStats } = res.data
//       if (user) {
//         !protectedRoutes && redirectUser(ctx, '/')
//       }
//       pageProps.user = user
//       pageProps.userFollowerStats = userFollowerStats
//     } catch (error) {
//       destroyCookie(ctx,'token')
//       redirectUser(ctx,'/login')
//     }
    
//   }
//   return {pageProps}
//   }

static async getInitialProps(appContext) {
    
  const { Component, ctx } = appContext
  let pageProps = {}

  // look for jwt token in the cookie
  const { token } = parseCookies(ctx)
  const protectedRoutes = (ctx.pathname === "/" || ctx.pathname === "/box")

    // if no token (not logged in) redirect them to login
  if (!token) {
    protectedRoutes && redirectUser(ctx, "/login")
  } else { // else - allow them to get props
  if(Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx)
  }

  try {
    const res = await axios.get(`${baseUrl}/api/auth`,{headers:{Authorization: token}})
    const { user } = res.data
    if (user) {
      !protectedRoutes && redirectUser(ctx, '/box')
    }
    pageProps.user = user
  } catch (error) {
    destroyCookie(ctx,'token')
    redirectUser(ctx,'/login')
  }
  
}
return {pageProps}
}

  render() {
    const { Component, pageProps } = this.props;
    return (
      <Grommet full theme={theme} {...pageProps}>
        <Layout {...pageProps}>
          <Component {...pageProps} />
        </Layout>

      </Grommet>
    );
  }
}

export default MyApp;
